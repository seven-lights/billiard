<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions;


use general\controllers\api\Controller;

/**
 * Контроллер-родитель для api-контроллеров
 */
class ApiController extends Controller {
	/* public function beforeAction($action) {
		if (parent::beforeAction($action)) {
			return true;
		} else {
			return false;
		}
	} */
} 