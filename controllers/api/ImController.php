<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\Message;
use general\controllers\api\Controller;
use yii\filters\AccessControl;

class ImController extends ApiController {
    protected $_safe_actions = ['get-dialogs', 'send', 'get-messages', 'has-new'];
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['get-dialogs', 'send', 'get-messages', 'has-new'],
				'rules' => [
					[
						'actions' => ['get-dialogs', 'send', 'get-messages', 'has-new'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
    public function actionHasNew() {
        $model = Message::find()
            ->where(['message.owner_id' => \Yii::$app->user->id, 'message.type' => 'in', 'is_read' => false])
            ->limit(1)
            ->one();

        return $this->sendSuccess([
            'has_new' => (bool)$model
        ]);
    }
    public function actionGetMessages($interlocutor_id, $last_id = null) {
        $model = Message::find()
            ->where(['message.owner_id' => \Yii::$app->user->id, 'message.interlocutor_id' => $interlocutor_id]);

        if($last_id) {
            $model = $model->andWhere(['>', 'message.id', (int)$last_id]);
        }

        $model = $model->joinWith('interlocutor')
            ->orderBy(['message.created_at' => SORT_ASC])
            ->all();

        $messages = [];
        /** @var Message $item */
        foreach ($model as $item) {
            $messages[] = [
                'id' => $item->id,
                'text' => $item->text,
                'name' => $item->type == 'in' ? $item->interlocutor->nick : null,
                'type' => $item->type == 'in' ? 'received' : 'sent',
                'day' => date('d.m.Y', $item->created_at),
                'time' => date('H:i', $item->created_at),
            ];

            if(!$item->is_read) {
                $item->is_read = true;
                $item->save();
            }
        }

        return $this->sendSuccess([
            'messages' => $messages
        ]);
    }
    public function actionSend($interlocutor_id)
    {
        if (\Yii::$app->request->isPost) {
            $transaction = \Yii::$app->getDb()->beginTransaction();
            try {
                $model = new Message();
                $model->loadDefaultValues();
                $model->interlocutor_id = $interlocutor_id;
                $model->owner_id = \Yii::$app->user->id;
                $model->type = 'out';
                $model->setAttributes(\Yii::$app->request->post());
                $model->save();

                $model = new Message();
                $model->loadDefaultValues();
                $model->interlocutor_id = \Yii::$app->user->id;
                $model->owner_id = $interlocutor_id;
                $model->type = 'in';
                $model->setAttributes(\Yii::$app->request->post());
                $model->save();

                $transaction->commit();
                return $this->sendSuccess([]);
            } catch(\Exception $e) {
                $transaction->rollBack();
            }

        }
        return $this->sendError(Controller::ERROR_DB);
    }
    public function actionGetDialogs() {
        $model = Message::find()
			->where(['message.owner_id' => \Yii::$app->user->id, 'message.type' => 'in'])
            ->joinWith('interlocutor')
			->groupBy('message.interlocutor_id')
			->orderBy(['message.created_at' => SORT_ASC])
            ->all();

        $dialogs = [];
        /** @var Message $item */
        foreach ($model as $item) {
            $dialogs[] = [
                'interlocutor_id' => $item->interlocutor_id,
                'nick' => $item->interlocutor->nick,
                'count' => Message::find()
                    ->where([
                        'owner_id' => \Yii::$app->user->id,
                        'type' => 'in',
                        'interlocutor_id' => $item->interlocutor_id,
                        'is_read' => false
                    ])->count(),
            ];
        }

        return $this->sendSuccess([
            'dialogs' => $dialogs
        ]);
    }
}