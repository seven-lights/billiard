<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\ResultOfTask;
use app\models\Task;
use app\models\User;
use yii\base\Exception;
use yii\db\Query;
use yii\filters\AccessControl;

class RatingController extends ApiController {
    protected $_safe_actions = ['get'];
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['get'],
				'rules' => [
					[
						'actions' => ['get'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
    public function actionGet() {
	    $model = User::find()
            ->select(['id', 'nick', 'rating'])
            ->orderBy('rating')
            ->asArray()
            ->all();

        return $this->sendSuccess([
            'users' => $model
        ]);
    }
}