<?php

namespace app\controllers;

use app\models\Message;
use yii\filters\AccessControl;

class MessageController extends \yii\web\Controller
{
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['dialog'],
				'rules' => [
					[
						'actions' => ['dialog'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
    public function actionDialog($id)
    {
	    if (\Yii::$app->request->isPost) {
		    $transaction = \Yii::$app->getDb()->beginTransaction();
		    try {
			    $model = new Message();
			    $model->loadDefaultValues();
			    $model->interlocutor_id = $id;
			    $model->owner_id = \Yii::$app->user->id;
			    $model->type = 'out';
			    $model->setAttributes(\Yii::$app->request->post());
			    $model->save();

			    $model = new Message();
			    $model->loadDefaultValues();
			    $model->interlocutor_id = \Yii::$app->user->id;
			    $model->owner_id = $id;
			    $model->type = 'in';
			    $model->setAttributes(\Yii::$app->request->post());
			    $model->save();

			    $transaction->commit();
			    return $this->refresh();
		    } catch(\Exception $e) {
			    $transaction->rollBack();
		    }

	    }
        return $this->render('dialog', ['id' => $id]);
    }
}
