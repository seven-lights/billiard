<?php

use yii\db\Schema;
use yii\db\Migration;

class m141029_175010_create_auth_key_table extends Migration
{
    public function up()
    {
	    $this->createTable('auth_key', [
		    'auth_key' => 'CHAR(64) PRIMARY KEY',
		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'browser' => Schema::TYPE_STRING . ' NOT NULL',
		    'ip' => Schema::TYPE_STRING . '(15) NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('user_id_FK_auth_key', 'auth_key', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m141029_175010_create_auth_key_table cannot be reverted.\n";

        return false;
    }
}
