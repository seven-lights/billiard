<?php
use general\widgets\api\LoginForm;

$this->title = 'Аутентификация';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
	#login {
		border: 0 none;
		display: block;
		height: 500px;
		margin: 5% auto 0;
		width: 340px;
		max-width: 90%;
	}
</style>
<?= LoginForm::widget([
	'service' => Yii::$app->id,
	'view' => 'login',
	'retUrl' => '//' . Yii::$app->params['api']['loginUrls'][ Yii::$app->id ],
	'class' => 'login',
	'id' => 'login',
]);
?>