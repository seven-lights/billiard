<?php
/* @var $this yii\web\View */
$this->title = 'Тренировка';

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>My App</title>
    <!-- Path to Framework7 Library CSS-->
    <link rel="stylesheet" href="css/framework7.ios.min.css">
    <link rel="stylesheet" href="css/framework7.ios.colors.min.css">
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="css/my-app.css">
    <style>
        .task-wrap {
            position: relative;
            z-index: 15;
        }

        .task {
            background-color: #dddddd;
            padding: 0.5em;
            margin-right: 2em;
        }

        #complexity {
            color: white;
            text-align: center;
            padding: 0.5em;
            float: right;
            font-weight: bold;
        }

        .pockets {
            width: 1.96%;
            height: 4%;
            position: absolute;
            background-color: rgb(16, 16, 16);
            border-radius: 50%;
            box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.9);
            z-index: 20;
        }

        #pocket0 {
            top: -0.8%;
            left: -0.3%;
        }

        #pocket1 {
            top: -1.2%;
            left: 48.92%;
        }

        #pocket2 {
            top: -0.8%;
            left: 98.34%;
        }

        #pocket3 {
            top: 96.8%;
            left: 98.34%;
        }

        #pocket4 {
            top: 96.8%;
            left: 48.92%;
        }

        #pocket5 {
            top: 96.8%;
            left: -0.3%;
        }

        .pockets.active {
            background-color: yellow;
        }

        .board {
            position: absolute;
        }

        .balls {
            width: 1.96%;
            height: 4%;
            position: absolute;
            z-index: 20;
            border-radius: 50%;
        }

        #trajectory {
            width: 100%;
            height: 100%;
        }

        .marks {
            width: 100%;
            margin: 0;
            background-color: #efeff4;
            position: relative;
            z-index: 15;
        }

        #mark input {
            position: relative;
            z-index: 2;
        }

        #mark input, #mark button {
            font-size: 1.1em;
            padding: .1em;
            /* width: 100%; */
            box-sizing: border-box;
        }

        #mark button {
            /* width: 50%; */
            float: left;
        }

        #skip, #result1, #result3, #result5, #result10 {
            width: 18%;
            z-index: 2;
            display: inline-block;
            margin: 1%;
        }

        .new-message .im {
            animation-name: emergence;
            animation-duration: 1s;
            animation-iteration-count: infinite;
        }

        @keyframes emergence {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        .ball-for-target {
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -moz-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -o-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            border-radius: 50%;
            background: radial-gradient(circle at 50% 0%, #dc0000, #ba0000 10%, #800000 80%, #640000 100%)
        }

        .ball-for-target .target {
            margin-left: 50%;
            margin-top: 50%;
            -webkit-transform: translate(-50%, -50%);
            -moz-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -o-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        .panel .menu {
            margin: 0;
        }
    </style>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/glyphicon.css">
</head>
<body>
<!-- Status bar overlay for fullscreen mode-->
<div class="statusbar-overlay"></div>
<!-- Panels overlay-->
<div class="panel-overlay"></div>
<!-- Left panel with reveal effect-->
<div class="panel panel-left panel-reveal layout-dark">
    <div class="list-block menu">
        <ul>
            <li>
                <div class="item-content">
                    <div class="item-media"><i class="glyphicon glyphicon-user"></i></div>
                    <div class="item-inner">
                        <div class="item-title"><?= Yii::$app->user->identity->nick ?></div>
                        <a href="/site/logout" class="item-link external">
                            <i class="glyphicon glyphicon-log-out"></i>
                        </a>
                    </div>
                </div>
            </li>
            <li>
                <div class="item-content">
                    <div class="item-inner"></div>
                </div>
            </li>
            <li>
                <a href="training.html" class="item-link close-panel">
                    <div class="item-content">
                        <div class="item-media"><i class="fa fa-flag"></i></div>
                        <div class="item-inner">
                            <div class="item-title">Тренировка</div>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="rating.html" class="item-link close-panel">
                    <div class="item-content">
                        <div class="item-media"><i class="fa fa-bar-chart"></i></div>
                        <div class="item-inner">
                            <div class="item-title">Рейтинг</div>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="dialogs.html" class="item-link close-panel">
                    <div class="item-content">
                        <div class="item-media"><i class="fa fa-envelope-o"></i></div>
                        <div class="item-inner">
                            <div class="item-title">Сообщения</div>
                        </div>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- Views-->
<div class="views">
    <!-- Your main view, should have "view-main" class-->
    <div class="view view-main">
        <!-- Top Navbar-->
        <div class="navbar">
            <div class="navbar-inner">
                <div class="left">
                    <a href="#" class="link icon-only open-panel"><i class="icon icon-bars"></i></a>
                </div>
                <!-- We have home navbar without left link-->
                <div class="center sliding">Загрузка</div>
                <div class="right">
                    <a href="dialogs.html" class="link"><i class="fa fa-envelope-o"></i></a>
                </div>
            </div>
        </div>
        <!-- Pages, because we need fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages navbar-through toolbar-through">
            <div data-page="training" class="page">
                <div class="page-content">
                    <span style="width:42px; height:42px;margin: 10px auto 0;display: block;" class="preloader"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Path to Framework7 Library JS-->
<script type="text/javascript" src="js/framework7.js"></script>
<!-- Path to your app js-->
<script type="text/javascript" src="js/my-app.js"></script>
</body>
</html>