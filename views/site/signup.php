<?php
use general\widgets\api\SignupForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
	<style>
		#signup {
			border: 0 none;
			display: block;
			height: 500px;
			margin: 5% auto 0;
			width: 340px;
			max-width: 90%;
		}
	</style>
<?= SignupForm::widget([
	'service' => Yii::$app->id,
	'view' => 'signup',
	'retUrl' => '//' . Yii::$app->params['api']['loginUrls'][ Yii::$app->id ],
	'class' => 'signup',
	'id' => 'signup',
]);
?>