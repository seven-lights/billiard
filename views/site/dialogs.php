<?php
/* @var $this yii\web\View */
use app\models\Message;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

$this->title = 'Сообщения';

$dataProvider = new ActiveDataProvider([
	'query' => Message::find()
		->where(['owner_id' => Yii::$app->user->id])
		->with('interlocutor')
		->groupBy('interlocutor_id')
		->orderBy(['created_at' => SORT_DESC]),
	'pagination' => [
		'pageSize' => 20,
	],
]);

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'layout' => "{items}\n{pager}",
	'showHeader' => false,
	'columns' => [
		[
			'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
			'value' => function ($data) {
				return '<a href="' . Yii::$app->urlManager->createUrl(['message/dialog', 'id' => $data->interlocutor_id]) . '">' . $data->interlocutor->nick . '</a>';
			},
			'format' => 'raw',
		],
		[
			'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
			'value' => function ($data) {
				if($data->type == 'in' && !$data->is_read) {
					return '<a href="' . Yii::$app->urlManager->createUrl(['message/dialog', 'id' => $data->interlocutor_id]) . '" class="fa fa-envelope-o"></a>';
				}
				return '';
			},
			'format' => 'raw',
		],
	],
]);
?>
<style>
	.table {
		width: 100%;
		text-align: center;
	}
</style>
