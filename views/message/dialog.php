<?php
/* @var $this yii\web\View */
use app\models\Message;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

$this->title = 'Сообщения';

$dataProvider = new ActiveDataProvider([
	'query' => Message::find()
		->where(['owner_id' => Yii::$app->user->id, 'interlocutor_id' => $id])
		->with('interlocutor')
		->orderBy(['created_at' => SORT_DESC]),
	'pagination' => [
		'pageSize' => 20,
	],
]);

echo ListView::widget([
	'dataProvider' => $dataProvider,
	'itemView' => '_dialog',
	'layout' => "{items}\n{pager}",
]);

$form = \yii\widgets\ActiveForm::begin([
	'id' => 'post-form',
]);
echo \yii\helpers\Html::textarea('text', '');
echo \yii\helpers\Html::submitButton();
\yii\widgets\ActiveForm::end();
?>
<style>
	.sendMessage {
		position: fixed; /* Фиксированное положение */
		left: 0; bottom: 0; /* Левый нижний угол */
		padding: 10px; /* Поля вокруг текста */
		background: #39b54a; /* Цвет фона */
		color: #fff; /* Цвет текста */
		width: 100%; /* Ширина слоя */
	}
</style>